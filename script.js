// Types of user
const ACADAMIC_STAFF = 'acadamicStaff';
const STUDENT = 'student';

// Acadamic staff roles
const UNIT_COORDINATOR = 'unitCordinator';
const LECTURER = 'lecturer';
const TUTOR = 'tutor';

let users = [
    {
        name: 'Aabhas Karmacharya',
        type: ACADAMIC_STAFF,
        role: UNIT_COORDINATOR,
        id: 1,
        email: 'aabhas@test.com',
        password: 'password',
        address: 'Here is my address',
        qualification: 'phd',
        expertise: 'Information Systems',
        phoneNumber: '+977-9876543210'
    },
    {
        name: 'Omer Ben Nahum',
        type: ACADAMIC_STAFF,
        role: UNIT_COORDINATOR,
        id: 2,
        email: 'omer@test.com',
        password: 'password',
        address: 'Here is my address',
        qualification: 'phd',
        expertise: 'Information Systems',
        phoneNumber: '+977-9876543210'
    },
    {
        name: 'Govinda Bhandari',
        type: STUDENT,
        id: 3,
        email: 'govinda@test.com',
        password: 'password',
        address: 'Here is my address', // optional
        DateOfBirth: 'timestap', // optional
        phoneNumber: '+977-9876543210' // optional
    }
]

function loadUserList() {
    let userList = document.getElementById('userList');
    let selectAcadamicStaff = document.getElementById('selectAcadamicStaff');

    // clearing/re-setting user list innerHtml to avoid duplication while multiple call
    userList.innerHTML = '';
    selectAcadamicStaff.innerHTML = '<option value="" disabled selected>Select Lecture</option>';

    // filtering acadamic staff
    users = users.filter(user => user.type === ACADAMIC_STAFF);

    // inserting user element to list
    users.forEach((user, i) => {
        // logic for display acadamic staff in table
        let listItem = document.createElement('tr');
        listItem.innerHTML = `
            <th scope="row">${i + 1}</th>
            <td>${user.id}</td>
            <td>${user.name}</td>
            <td>${user.email}</td> 
        `;
        userList.append(listItem);

        // logic for display acadamic staff in select option
        let optionItem = document.createElement('option');
        optionItem.setAttribute("value", user.id); // setting attribute eg: <option value=user.aid></option>

        optionItem.innerHTML = user.name;

        selectAcadamicStaff.append(optionItem);
    });
}
loadUserList();



//UNITS LOGIC
let units = [
    {
        name: 'Unit 1',
        code: 'CODE HERE',
        creditPoint: 'POINT HERE',
        description: 'Here is the unit description 1',
        lecturerId: 1 // lecturer id
    },
    {
        name: 'Unit 2',
        code: 'CODE HERE',
        creditPoint: 'POINT HERE',
        description: 'Here is the unit description 2',
        lecturerId: 2 // lecturer id
    },
    {
        name: 'Unit 3',
        code: 'CODE HERE',
        creditPoint: 'POINT HERE',
        description: 'Here is the unit description 3',
        lecturerId: 1 // lecturer id
    }
]

const getLecturerNameById = (id) => {
    let { name } = users.filter(user => user.id == id)[0] || {};
    return name;
}

let unitList = document.getElementById('unitList');

let lastIndex;
function appendUnitList(unit, i = lastIndex) {
    // logic for display units in table
    let listItem = document.createElement('tr');
    listItem.innerHTML = `
            <th scope="row">${i + 1}</th>
            <td>${unit.name}</td>
            <td>${unit.code}</td>
            <td>${unit.creditPoint}</td>
            <td>${unit.description}</td>
            <td>${getLecturerNameById(unit.lecturerId)}</td>
            <td><button class="btn btn-sm btn-success">Enroll now</button><td> 
        `;
    unitList.append(listItem); // use prepend for oppsite list
    lastIndex = i + 1; // we need to store last index so that we can continue indexing new data
}

function loadUnitList() {
    // clearing unit list innerHtml to avoid duplication while multiple call
    unitList.innerHTML = '';

    // inserting unit element to list
    units.forEach((unit, i) => {
        appendUnitList(unit, i)
    });
}
loadUnitList();

const unitForm = document.getElementById('unitForm');
unitForm.addEventListener('submit', (event) => {
    event.preventDefault();

    // implement JS validation logic HERE

    const newUnit = {
        name: unitName.value,
        description: unitDescription.value,
        code: unitCode.value,
        creditPoint: unitCreditPoint.value,
        lecturerId: selectAcadamicStaff.value
    }

    units.push(newUnit);
    appendUnitList(newUnit);

    //clearing field after success
    unitName.value = '';
    unitDescription.value = '';
    unitCode.value = '';
    unitCreditPoint.value = '';
    selectAcadamicStaff.value = '';
});
